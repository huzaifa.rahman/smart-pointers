#include "student.h"
#include "database.h"

/*
CORNER CASES:
    When wrong name is given to get_student_reference function
    When nullptr is sent to print_student_record for print data
*/

using namespace std;
using namespace emumba::training;

//  print function to print student info
void print_student_record(const unique_ptr<student> &student_ptr);

int main()
{
    database student_database;
    //  accessing student name using shared pointer
    printf("%-10s : %s\n", student_database.get_student_shared_reference("ali")->getName().c_str(), "hello! I came from a \"shared\" pointer function.");
    //  accessing student name using unique pointer
    printf("%-10s : %s\n", student_database.get_student_unique_reference("huzaifa")->getName().c_str(), "hello! I came from a \"unique\" pointer function.");

    // exception case handling
    if (student_database.get_student_shared_reference("unkown-name") != nullptr)
    {
        printf("%-10s : %s\n", student_database.get_student_shared_reference("ali")->getName().c_str(), "hello from shared pointer function.");
    }
    else
    {
        printf("ERROR! === Function : %s === Name not found in student list.\n", __FUNCTION__);
    }

    print_student_record(student_database.get_student_unique_reference("huzaifa"));
    // exception case
    print_student_record(student_database.get_student_unique_reference("unknown-name"));

    return 0;
}

void print_student_record(const unique_ptr<student> &student_ptr)
{
    if (student_ptr != nullptr) //  exception if pointer points to nulls
        printf("=====================================\n"
               "%4s : %-8s %8s : %-4s %5s : %-5.2f %5s : %-5d\n"
               "=====================================\n",
               "Name", student_ptr->getName().c_str(),
               "Roll#", student_ptr->getRollNo().c_str(),
               "CGPA", student_ptr->getCGPA(),
               "Age", student_ptr->getAge());
    else
        printf("ERROR! === Function : %s === ERROR!  Student pointer is not pointing to a valid data.\n", __FUNCTION__);
}