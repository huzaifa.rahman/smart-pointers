# Smart Pointer

This repo is for practicing smart pointer concepts.

### How to configure, build and run

**Configure** :
- Clone this repo using ``git clone https://gitlab.com/huzaifa.rahman/smart-pointers.git`` and go to the newly created local dir with ``cd``.
- Run ``mkdir build`` to create a build dir for configuration and build files. 
- Navigate to ``<project_root_dir>/build``.
- Run ``cmake ..`` to congigure in ``Release`` or ``cmake -DCMAKE_BUILD_TYPE=Debug ..`` to congigure in ``Debug`` and ``Release`` mode.

**Build** : 
- In ``<project_root_dir>/build`` run ``make`` build the executable or run ``cmake --build .``.

**Execute / Run** : 
- In ``<project_root_dir>/build`` run ``./student_entry`` or ``./student_entry_debug`` to execute in release or debug executable.

### Task 1 and 2 : Using previous code base

For this task a standard cmake file structure is re-used from previous assignment with slight modifications which are: 
- Extra/un-used code is removed
- Comments are added where necessary
- Attempt is made to produce more readable/efficient code 

### Task 3 : Declare all classes in ``emumba::training`` namespace

For this task a namespace named ``emumba::training`` is defined in ``student.h`` file. Then this namespace is then used in ``main.cpp``, ``student.cpp`` and ``database.cpp`` files.

### Task 4 : Create a ``database`` class

The following is the structure of this class
- Constructor : ``database()`` which initializes a vector of data type ``student``.
- Member functions : ``get_student_reference`` and ``get_student_unque_reference`` which return shared and unique pointers of requested student entry. 
- Data member : ``student_list``, a vector of ``student`` type.

### Task 5 : Write a function that returns a ``shared_ptr`` of ``student`` data type

``get_student_reference`` function returns a shared pointer to the ``student_list`` element which holds the data specified in ``student_name`` argument.

### Task 6 : Write a function that returns a ``unique_ptr`` of ``student`` data type

``get_student_unque_reference`` function returns a unique pointer to the ``student_list`` element which holds the data specified in ``student_name``<br/>
**Note:** Functions cannot overloaded/distinguished by return type alone.

### Task 7 : Write a print function which print data pointed by a unique pointer

``print_student_record`` function is used for this purpose which inputs unique pointer and outputs/prints the data pointed by this pointer.

### Task 8 : Handle corner cases

Handled corner cases are:
- When unkown name is passed to ``get_student_reference`` or ``get_student_unque_reference`` functions then they will return a ``nullptr`` to indicate that the required entry doesn`t exist. And appropriate error message will be shown.
- When ``nullptr`` is sent to ``print_student_record`` for printing data then it will show appropriate error message.

### Task 9 : Make argument and functions ``const`` where required

This is done for all use cases (I suppose ;)














