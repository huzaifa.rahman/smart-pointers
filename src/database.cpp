#include "database.h"

using namespace std;
using namespace emumba::training;

database::database()
{
    student_list.reserve(5);
    student student1("huzaifa", "1", 20 + 1, 1.5 + .1),
        student2("ali", "2", 20 + 2, 1.5 + .2),
        student3("zaid", "3", 20 + 3, 1.5 + .3),
        student4("anas", "4", 20 + 4, 1.5 + .4),
        student5("usama", "5", 20 + 5, 1.5 + .5);
    student_list = {student1, student2, student3, student4, student5};
}
shared_ptr<student> database::get_student_shared_reference(const string &student_name) const
{
    for (const auto &student_entry : student_list)
    {
        if (!student_entry.getName().compare(student_name))
        {
            return make_shared<student>(student_entry);
        }
    }
    return nullptr;
}
unique_ptr<student> database::get_student_unique_reference(const string &student_name) const
{
    for (const auto &student_entry : student_list)
    {
        if (student_entry.getName().compare(student_name) == 0)
        {
            return make_unique<student>(student_entry);
        }
    }
    return nullptr;
}