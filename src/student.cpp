#include "student.h"

using namespace std;
using namespace emumba::training;

student::student(const string &name, const string &roll_no, const int &age, const float &cgpa)
{
  student::setName(name);
  student::setRollNo(roll_no);
  student::setAge(age);
  student::setCGPA(cgpa);
}
student::~student() {}
const string student::getName() const
{
  return record.name;
}
void student::setName(const string &name)
{
  record.name = name;
}
const string student::getRollNo() const
{
  return record.roll_no;
}
void student::setRollNo(const string &roll_no)
{
  record.roll_no = roll_no;
}
int student::getAge() const
{
  return record.age;
}
void student::setAge(const int &age)
{
  record.age = age;
}
const float student::getCGPA() const
{
  return record.cgpa;
}
void student::setCGPA(const float &cgpa)
{
  record.cgpa = cgpa;
}
