#ifndef _DATABASE_H
#define _DATABASE_H

#include "student.h"
#include <string>
#include <vector>
#include <memory>

class emumba::training::database
{
private:
  std::vector<emumba::training::student> student_list;

public:
  database();
  std::shared_ptr<emumba::training::student> get_student_shared_reference(const std::string &student_name) const;
  // cannot overload functions distinguished by return type alone
  std::unique_ptr<emumba::training::student> get_student_unique_reference(const std::string &student_name) const;
};

#endif