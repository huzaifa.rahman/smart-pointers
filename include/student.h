#ifndef _STUDENT_H
#define _STUDENT_H

#include <string>
#include <vector>
#include <memory>
namespace emumba
{
  namespace training
  {
    class student;
    class database;
  } // namespace training
} // namespace emumba

class emumba::training::student
{
private:
  struct student_record
  {
    std::string name;
    std::string roll_no;
    int age;
    float cgpa;
  } record;

public:
  student(const std::string &name, const std::string &roll_no, const int &age, const float &cgpa);
  ~student();
  const std::string getName() const;
  void setName(const std::string &name);
  const std::string getRollNo() const;
  void setRollNo(const std::string &roll_no);
  int getAge() const;
  void setAge(const int &age);
  const float getCGPA() const;
  void setCGPA(const float &cgpa);
};

#endif